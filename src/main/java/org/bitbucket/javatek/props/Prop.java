package org.bitbucket.javatek.props;

import java.math.BigDecimal;
import java.util.Objects;

import static org.bitbucket.javatek.require.ObjectRequires.requireNonNull;
import static org.bitbucket.javatek.require.StringRequires.requireNonEmpty;

/**
 *
 */
public final class Prop {
  private final String key;
  private final Boolean bool;
  private final String string;
  private final BigDecimal number;

  Prop(String key, String string) {
    this.key = requireNonEmpty(key);
    this.bool = null;
    this.string = requireNonNull(string);
    this.number = null;
  }

  Prop(String key, BigDecimal number) {
    this.key = requireNonEmpty(key);
    this.bool = null;
    this.string = null;
    this.number = requireNonNull(number);
  }

  Prop(String key, boolean bool) {
    this.key = requireNonEmpty(key);
    this.bool = bool;
    this.string = null;
    this.number = null;
  }

  public String key() {
    return key;
  }

  public String asString() throws ElementCastException {
    if (string != null)
      return string;
    if (number != null)
      return number.toPlainString();
    if (bool != null)
      return bool.toString();
    throw new ElementCastException("String expected");
  }

  public BigDecimal asNumber() throws ElementCastException {
    if (number != null)
      return number;
    throw new ElementCastException("Number expected");
  }

  public boolean asBoolean() throws ElementCastException {
    if (bool != null)
      return bool;
    throw new ElementCastException("Boolean expected");
  }

  @Override
  public String toString() {
    if (string != null)
      return key + " -> " + string;
    if (number != null)
      return key + " -> " + number.toPlainString();
    if (bool != null)
      return key + " -> " + (bool ? "true" : "false");
    throw new AssertionError();
  }

  @Override
  public int hashCode() {
    return Objects.hash(bool, string, number);
  }

  @Override
  public boolean equals(Object obj) {
    return obj == this ||
      obj != null && obj.getClass() == Prop.class
        && Objects.equals(((Prop) obj).key, this.key)
        && Objects.equals(((Prop) obj).bool, this.bool)
        && Objects.equals(((Prop) obj).string, this.string)
        && Objects.equals(((Prop) obj).number, this.number)
      ;
  }
}
