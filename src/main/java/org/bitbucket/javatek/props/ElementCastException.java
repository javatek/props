package org.bitbucket.javatek.props;

/**
 *
 */
public final class ElementCastException extends RuntimeException {
  private static final long serialVersionUID = 6873664682944870144L;

  ElementCastException(String message) {
    super(message);
  }
}
