package org.bitbucket.javatek.props;

import javax.annotation.Nonnull;
import java.math.BigDecimal;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.StringJoiner;

import static java.util.Collections.emptyMap;
import static org.bitbucket.javatek.require.ObjectRequires.requireNonNull;

/**
 *
 */
public final class Props implements Iterable<Prop> {
  public static final Props NO_PROPS = new Props();

  private final Map<String, Prop> props;

  public Props() {
    this(emptyMap());
  }

  private Props(Map<String, Prop> props) {
    this.props = requireNonNull(props);
  }

  @Nonnull
  public Props withString(@Nonnull String key, @Nonnull String string) {
    return withProp(key, new Prop(key, string));
  }

  @Nonnull
  public Props withNumber(@Nonnull String key, @Nonnull BigDecimal number) {
    return withProp(key, new Prop(key, number));
  }

  @Nonnull
  public Props withBoolean(@Nonnull String key, boolean bool) {
    return withProp(key, new Prop(key, bool));
  }

  private Props withProp(String key, Prop prop) {
    requireNonNull(key);
    requireNonNull(prop);
    Map<String, Prop> newProps = new LinkedHashMap<>(props);
    newProps.put(key, prop);
    return new Props(newProps);
  }

  //////////////////////////////////////////////////////////////////////////////

  public boolean has(@Nonnull String key) {
    requireNonNull(key);
    return props.containsKey(key);
  }

  @Nonnull
  public String getString(@Nonnull String key) throws NoSuchElementException, ElementCastException {
    return getProp(key).asString();
  }

  @Nonnull
  public BigDecimal getNumber(@Nonnull String key) throws NoSuchElementException, ElementCastException {
    return getProp(key).asNumber();
  }

  public boolean getBoolean(@Nonnull String key) throws NoSuchElementException, ElementCastException {
    return getProp(key).asBoolean();
  }

  @Nonnull
  public Prop getProp(@Nonnull String key) throws NoSuchElementException {
    requireNonNull(key);
    Prop prop = props.get(key);
    if (prop == null)
      throw new NoSuchElementException(key + " not found");
    return prop;
  }

  @Nonnull
  @Override
  public Iterator<Prop> iterator() {
    return props.values().iterator();
  }

  @Override
  public String toString() {
    StringJoiner joiner = new StringJoiner(", ", "{", "}");
    for (Prop prop : this)
      joiner.add(prop.toString());
    return joiner.toString();
  }

  @Override
  public int hashCode() {
    return Objects.hash(props);
  }

  @Override
  public boolean equals(Object obj) {
    return obj == this ||
      obj != null && obj.getClass() == Props.class
        && ((Props) obj).props.equals(this.props)
      ;
  }
}
